var util = require("util");
var fs = require('fs');

var Cars = require('./myCars');
var Track = require('./myTrack');
var Race = require('./myRace');





/***
 *    #### ##    ## #### ########
 *     ##  ###   ##  ##     ##
 *     ##  ####  ##  ##     ##
 *     ##  ## ## ##  ##     ##
 *     ##  ##  ####  ##     ##
 *     ##  ##   ###  ##     ##
 *    #### ##    ## ####    ##
 */

/**
 *
 * @constructor
 */
var Hwo = function () {
    this.serverHost = process.argv[2];
    this.serverPort = process.argv[3];
    this.botName = process.argv[4];
    this.botKey = process.argv[5];
    this.environment = process.argv[6] || "C.I.";
    this.carCount = parseInt(process.argv[7]) || 1;
    this.ownCarColor = "";

    this.angleDataOk = false;

    this.gameInit = false;
    this.isQualification = false;
    this.isRace = false;

    // switch logic
    this.switch = {};
    this.switch.left = false;
    this.switch.right = false;
    this.switch.sent = false;
    this.lanesTaken = [];

    // crash logic
    this.willCrash = false;
    this.safeAngle = 60;

    if (this.environment === "TEST" && this.botName === "PetrolHead") {this.safeAngle = 60}
    if (this.environment === "TEST" && this.botName === "Random60") {this.safeAngle = Math.random() * 60}
    if (this.environment === "TEST" && this.botName === "Steady20") {this.safeAngle = 20}
    if (this.environment === "TEST" && this.botName === "Slow5") {this.safeAngle = Math.random() * 60}
    if (this.environment === "TEST" && this.botName === "Grandma") {this.safeAngle = 1}


    // SETUP
    this.doSwitches = false;
    this.doOvertakes = true;

}





/***
 *    ########  ########  ######  ########   #######  ##    ## ########
 *    ##     ## ##       ##    ## ##     ## ##     ## ###   ## ##     ##
 *    ##     ## ##       ##       ##     ## ##     ## ####  ## ##     ##
 *    ########  ######    ######  ########  ##     ## ## ## ## ##     ##
 *    ##   ##   ##             ## ##        ##     ## ##  #### ##     ##
 *    ##    ##  ##       ##    ## ##        ##     ## ##   ### ##     ##
 *    ##     ## ########  ######  ##         #######  ##    ## ########
 */
/**
 * Respond to server messages
 * @param message {Object} Message received.
 */
Hwo.prototype.respondToMessage = function (message) {
    if (message.msgType === 'carPositions') {
        if (typeof(message.gameTick) === 'undefined') {
            message.gameTick = 0
        }
        if (!this.cars.positionsInitialized) {
            this.cars.initData(message.data);
        }

        this.cars.updatePositions(message.data);

        if (this.cars.positionsInitialized && typeof this.track.pieces !== "undefined") {
            this.cars.calculateData(this.track);
        }

        if (!this.accelerationDataOk) {
            this.calculateSpeedMultipliers();
        }

        if (!this.angleDataOk) {
            this.calculateAngleMultipliers();
        }
        else {
                this.decideSwitch();
        }

        this.willCrash = this.gonnaCrash();
        this.decideThrottle(message.gameTick);

        this.calculatePieceMaxSlip();
        this.calculatePieceMaxSpeed();
        this.calculateLapMaxSlip();

// DEBUG LOGGING
// ====================
        var w = this.cars.car[0].angularSpeed[0];
        var a = this.cars.car[0].angularAcceleration[0];
        var pr = this.track.pieces[this.cars.car[0].pieceIndex[0]].laneRadius[this.cars.car[0].endLaneIndex[0]];
        var pa = this.track.pieces[this.cars.car[0].pieceIndex[0]].angle;
        var sw = (this.track.pieces[this.cars.car[0].pieceIndex[0]].switch)? "X" : "-";
        var gonnaCrash = (this.willCrash)? "OOPS!".red : "     ";
        if (!this.cars.car[0].isCrashed) {
            var tav = (this.cars.car[0].turboAvailable) ? "  TURBO ".yellow : "NO TURBO".grey;
            var tac = (this.cars.car[0].turboActive) ? "YES" : " NO";
            var ss = [];
            if (this.gameInit) {
                for (var l = 0; l < this.track.pieces[this.cars.car[0].pieceIndex[0]].laneSafeSpeed.length; l++) {
                    var temp = this.track.pieces[this.cars.car[0].pieceIndex[0]].laneSafeSpeed[l];
                    ss[l] = (temp < 1000) ? temp.toFixed(2) : "Inf";
                }
            }

            //debug(sprintf("Turbo[ Available: %s, Active: %s, Multiplier: %' 5.2f    %d..%d]", tav, tac, this.cars.car[0].turboMultiplier, this.track.maxStraightStart, this.track.maxStraightEnd));
            //debug(sprintf("P: %' 3d    T: %' 5.2f   V: %' 5.3f   A: %' 8.5f  w: %' 7.3f  a: %' 7.3f   R: %' 5.1f", this.cars.car[0].pieceIndex[0], this.cars.car[0].throttle, this.cars.car[0].speed[0], this.cars.car[0].angle[0], w, a, pr));
            //debug("-------------------------------------------------------------------------------------------------------")

            // Angle prediction

            p = this.cars.car[0].predictAngle(this.track, 0.0, 1);


            var aForce = (this.cars.car[0].angleParamA * this.cars.car[0].speed[0] * this.cars.car[0].speed[0] / Math.sqrt(pr) - this.cars.car[0].angleParamD * this.cars.car[0].speed[0]);
            aForce = Math.max(aForce, 0) * sign(pa); // Limit to 0..X
            var cForce = this.cars.car[0].angleParamC * this.cars.car[0].angle[0] * this.cars.car[0].speed[0];
            var bForce = this.cars.car[0].angleParamB * this.cars.car[0].angularSpeed[0];

            debug(sprintf("TICK %' 4d   [%' 3d| %' 2d |%' 6.2f] [  %' 5.2f | %' 5.3f]   [%' 9.5f]   [w: %' 7.3f  a: %' 7.3f]  [R: %' 8.1f A: %' 5.1f %s]".blue, message.gameTick, this.cars.car[0].pieceIndex[0], this.cars.car[0].lap[0], this.cars.car[0].inPieceDistance[0], this.cars.car[0].throttle, this.cars.car[0].speed[0], this.cars.car[0].angle[0], w, a, pr, pa, sw));
            debug(sprintf("%s       [   %s    ] [   %' 4.1f | %' 2d>%' 2d]".blue, gonnaCrash, tav, this.cars.car[0].turboMultiplier, this.track.maxStraightStart, this.track.maxStraightEnd));
            debug(sprintf("----------------------------------------------------%' 10s (%.2f) -------------------------------------------", this.botName, this.safeAngle));
            debug("------------[ PI|LAP | DIST ]-[    THR | SPEED]---[    ANGLE]---[      ANGULAR DATA    ]--[     PIECE DATA       ]");
            debug(sprintf("PREDICTION: [%' 3d|    |%' 6.2f] [  %' 5.2f | %' 5.3f]   [%' 9.5f]   [w: %' 7.3f  a: %' 7.3f]  [R: %' 8.1f A: %' 5.1f  ]".magenta, p.pieceIndex, p.inPieceDistance, p.throttle, p.speed, p.angle, p.angularSpeed, p.angularAcceleration, p.laneRadius, p.pieceAngle));

        }
        //if (this.cars.car[0].maxSpeed < 9.4 && this.cars.car[0].accelerationDataOk && message.gameTick * Math.random() > 150) {process.exit(0)}
        //if (this.cars.car[0].angleParamA > 0.47 && this.cars.car[0].angleDataOk && message.gameTick * Math.random() > 150) {process.exit(0)}
        //if (this.cars.car[0].lap[0]>5 && this.cars.car[0].inPieceDistance[0] > 40) {process.exit(0)}

// ========================





        if (this.switch.right) {
            this.send({
                msgType: "switchLane",
                data: "Right",
                gameTick: message.gameTick
            });
            this.switch.left = false;
            this.switch.right = false;
            this.switch.sent = true;
            debug("Right", "yellow");
        }
        else if (this.switch.left) {
            this.send({
                msgType: "switchLane",
                data: "Left",
                    gameTick: message.gameTick
            });
            this.switch.left = false;
            this.switch.right = false;
            this.switch.sent = true;
            debug("Left", "yellow");
        }
        else if (this.cars.car[0].turboAvailable && this.cars.car[0].pieceIndex[0] == this.track.maxStraightStart) {
            debug("Longest straight: "+this.track.maxStraightStart +".."+this.track.maxStraightEnd+", "+this.track.maxStraightLen);
            this.send({
                msgType: "turbo",
                data: "Hoooooly spiiiid!",
                gameTick: message.gameTick
            });
            this.cars.car[0].turboAvailable = false;
        }
        else {
        this.send({
            msgType: "throttle",
            data: this.cars.car[0].throttle,
            gameTick: message.gameTick
        });
    }
    //debug(message);
    }

    else if (message.msgType === 'join') {
        debug(message, "grey");
    }

    else if (message.msgType === 'yourCar') {
        this.cars = new Cars(message.data);
        debug(message);
    }

    else if (message.msgType === 'gameStart') {
        this.send({
            msgType: "throttle",
            data: 1.0,
            gameTick: message.gameTick
        });
        debug(message, "grey");
    }

    else if (message.msgType === 'gameEnd') {
        debug(message, "grey");
    }

    else if (message.msgType === 'turboAvailable') {
        for (var c = 0; c < this.cars.car.length; c++) {
            if (!this.cars.car[c].isCrashed) {
                this.cars.car[0].turboAvailable = true;
                this.cars.car[0].turboFactor = message.data.turboFactor;
            }
        }
        debug(message, "green");
    }

    else if (message.msgType === 'turboStart') {
        if (message.data.name == this.botName) {
            this.cars.car[this.cars.colorToIndex[message.data.color]].turboAvailable = false;
            this.cars.car[this.cars.colorToIndex[message.data.color]].turboActive = true;
        }
        debug(message, "green");
    }

    else if (message.msgType === 'turboEnd') {
        if (message.data.name == this.botName) {
            this.cars.car[this.cars.colorToIndex[message.data.color]].turboActive = false;
            this.cars.car[this.cars.colorToIndex[message.data.color]].turboAvailable = false;
        }
        debug(message, "green");
    }

    else if (message.msgType === 'crash') {
        if (message.data.name === this.botName) {
            this.cars.car[0].turboActive = false;
            this.cars.car[0].turboAvailable = false;
            //this.updateSafeSpeeds("crash");
            this.updateAngleParameters();
            //console.log('\u0007');
            if (this.cars.car[0].hasRammed) {debug("I suck at ramming.".red)}
            if (this.cars.car[0].wasRammed) {debug("Somebody sent me spinning".red)}
        }
        this.cars.car[this.cars.colorToIndex[message.data.color]].isCrashed = true;
        debug(message, "red");
    }

    else if (message.msgType === 'dnf' && this.cars.positionsInitialized) {
        this.cars.car[this.cars.colorToIndex[message.data.color]].isCrashed = true;
        debug(message, "red");
    }

    else if (message.msgType === 'spawn') {
        if (message.data.name == this.botName) {
            this.cars.car[0].turboActive = false;
            this.cars.car[0].turboAvailable = false;
        }
        debug(message, "red");
        this.cars.car[this.cars.colorToIndex[message.data.color]].isCrashed = false;
    }

    else if (message.msgType === 'gameInit') {
        if (!this.gameInit) {
            this.track = new Track(message.data.race.track);
            this.race = new Race(message.data.race.raceSession);
            this.cars.init(message.data.race.cars);
            this.gameInit = true;
        }
        if (typeof message.data.race.raceSession.durationMs !== "undefined") {
            this.isQualification = true;
            this.isRace = false;
            debug("============================");
            debug("Qualification");
            debug("============================");
        }
        if (typeof message.data.race.raceSession.laps !== "undefined") {
            this.isQualification = false;
            this.isRace = true;
            debug("============================");
            debug("Race");
            debug("============================");
        }

        this.cars.car[0].ticksSinceCrash = 0;

        //debug(this.track);
        debug(message);
    }
    else if (message.msgType === 'lapFinished') {
        if (message.data.car.name == this.botName) {
            //this.updateSafeSpeeds();
            this.cars.car[0].lapCrashes=0;
            this.maxSlipAngle=0;
            //debug(this.track);

            if (this.environment !== "C.I." && message.data.lapTime.lap > 0) {
                fs.appendFile("laps.txt", this.track.id + " Time: "+parseInt(message.data.lapTime.millis)/1000 + " maxSPD: " + this.cars.car[0].maxSpeed + " A: " + this.cars.car[0].angleParamA + "\n");
            }
        }

        debug(message);
    }
    else {
        if (
            message.gameTick
            && message.msgType === "carPositions"
            ) {
            /*
            this.send({
                msgType: "ping",
                data: {},
                gameTick: message.gameTick
            });
            */
        }
        debug(message);
    }
}









/**
 * Sends object as JSON to server
 * @param json {object} Object to send
 * @returns {*} Server response
 */
Hwo.prototype.send = function (json) {
    if (json.msgType !== "throttle") debug("Sending: "+json.msgType, "magenta");
    client.write(JSON.stringify(json));
    return client.write('\n');
};










/**
 * Prints structure of passed object
 * @param obj {*} Object to print structure of
 */
debug = function (obj, color) {
    color = color || "grey";
    if (hwo.environment === "C.I.") {
        if (typeof obj === "string") {
            console.log(obj);
        }
        else {
            console.log(util.inspect(obj, {showHidden: true, depth: null}));
        }
    }
    else {
        if (typeof obj === "string") {
            console.log(colors[color](obj));
        }
        else {
            console.log(colors[color](util.inspect(obj, {showHidden: true, depth: null, colors: true})));
        }
    }
}







/**
 * Returns -1 or 1 depending on sign of parameter
 * @param x {number}
 * @returns {number}
 */
sign = function (x) {
    return typeof x === 'number' ? x ? x < 0 ? -1 : 1 : x === x ? 0 : NaN : NaN;
}





/**
 * Returns logarithm of n in base b
 * @param b {number} Base b
 * @param n {number} Number n
 * @returns {number} Log b(n)
 */
log = function (b, n) {
    return Math.log(n) / Math.log(b);
}





/**
 * Adds 0 to start of array and shifts all values
 * @param storage {array} Input array to store values
 * @param n {number} Number of values to store.
 */
keepNLastValues = function (storage, n) {
    storage.unshift(0);
    for (var i = 0; i <= n - storage.length; i++)
    {
        storage.push(0);
    }
    if (storage.length > n + 1) {
        storage.splice(n - storage.length + 1, storage.length - n + 1);
    }
}







Hwo.prototype.calculateSpeedMultipliers = function() {
    this.cars.car[0].calculateSpeedMultipliers();

    // Update all other cars
    if (this.cars.car[0].accelerationDataOk) {
        for (var c = 1; c < this.cars.car.length; c++) {
            this.cars.car[c].initialAcceleration = this.initialAcceleration;
            this.cars.car[c].dragMultiplier = this.dragMultiplier;
            this.cars.car[c].accelerationDataOk = true;
        }
        this.accelerationDataOk = true;
    }
}





Hwo.prototype.calculateAngleMultipliers = function() {
    for (var c = 0; c < this.cars.car.length; c++) {
        this.cars.car[c].calculateAngleMultipliers();
        if (this.cars.car[c].angleDataOk) {
            for (var cc = 0; cc < this.cars.car.length; cc++) {
                this.cars.car[cc].angleParamA = this.cars.car[c].angleParamA;
                this.cars.car[cc].angleParamB = this.cars.car[c].angleParamB;
                this.cars.car[cc].angleParamC = this.cars.car[c].angleParamC;
                this.cars.car[cc].angleParamD = this.cars.car[c].angleParamD;
            }
            this.angleDataOk = true;
            break;
        }
    }
}



Hwo.prototype.nextCurveIndex = function(pieceIndex) {
    var pi = 0;
    var straightBeforeCorner = false;

    for (var p = 0; p < this.track.pieces.length; p++) {
        pi = this.track.getPieceIndexByOffset(pieceIndex, p);
        if (this.track.pieces[pi].laneRadius[0] === Infinity) {
            straightBeforeCorner = true;
        }
        if ( ( this.track.pieces[pi].laneRadius[0] !== this.track.pieces[pieceIndex].laneRadius[0]  || straightBeforeCorner )
            && this.track.pieces[pi].laneRadius[0] !== Infinity) {
            break;
        }
    }
    return pi;
}




Hwo.prototype.nextSwitchIndex = function(pieceIndex) {
    var pi = 0;

    for (var p = 0; p < this.track.pieces.length; p++) {
        pi = this.track.getPieceIndexByOffset(pieceIndex, p);
        if (this.track.pieces[pi].switch) {
            break;
        }
    }
    return pi;
}



Hwo.prototype.decideSwitch = function() {
    if (this.cars.car[0].pieceIndex[0] !== this.cars.car[0].pieceIndex[1] && this.switch.sent) {
        this.switch.sent = false;
    }


    //Normal switching
    else {
        var nextPieceIndex = this.track.getPieceIndexByOffset(this.cars.car[0].pieceIndex[0], 1);
        var nextPieceIsSwitch = this.track.pieces[nextPieceIndex].switch && this.track.pieces[nextPieceIndex].baseRadius >= 200;
        var willTravelThisFar = this.cars.car[0].inPieceDistance[0] + this.cars.car[0].predictDistanceAtThrottle(this.cars.car[0].speed[0], this.cars.car[0].speed[0], 2);

        var lastMoment = willTravelThisFar >= this.track.pieces[this.cars.car[0].pieceIndex[0]].laneLength[this.cars.car[0].startLaneIndex[0]];
        if (nextPieceIsSwitch && lastMoment && !this.switch.sent) {
            var shortestLane = this.shortestLaneToNextSwitch();
            var laneLeft = Math.max(0, this.cars.car[0].startLaneIndex[0]-1);
            var laneRight = Math.min(this.track.lanes.length - 1, this.cars.car[0].startLaneIndex[0]+1);
            if ( ( ( shortestLane > this.cars.car[0].startLaneIndex[0] && this.doSwitches ) || (this.stuckBehindSomeone && this.doOvertakes)) && this.lanesTaken.indexOf(laneRight) == -1) {
                this.switch.right = true;
            }
            if ( ( ( shortestLane < this.cars.car[0].startLaneIndex[0] && this.doSwitches ) || (this.stuckBehindSomeone && this.doOvertakes)) && this.lanesTaken.indexOf(laneLeft) == -1) {
                this.switch.left = true;
            }
        }
    }
};



Hwo.prototype.shortestLaneToNextSwitch = function() {
    var laneLeft = Math.max(0, this.cars.car[0].startLaneIndex[0]-1);
    var laneRight = Math.min(this.track.lanes.length - 1, this.cars.car[0].startLaneIndex[0]+1);
    var shortestLaneLength = 0;
    var shortestLane = this.cars.car[0].startLaneIndex[0];


    for (var l = laneLeft; l <= laneRight; l++) {
        var s = 0;
        var laneLength = 0;
        for (var p = 0; p < this.track.pieces.length; p++) {
            pi = this.track.getPieceIndexByOffset(this.cars.car[0].pieceIndex[0], p);
            if (this.track.pieces[pi].switch && this.track.pieces[pi].baseRadius >= 200) {
                s++
            }
            if (s > 1) {
                break;
            }
            laneLength += this.track.pieces[pi].laneLength[l];
        }
        if (shortestLaneLength == 0 || laneLength < shortestLaneLength) {
            shortestLaneLength = laneLength;
            shortestLane = l;
        }
    }
    return shortestLane;
}




// returns number of switches available before pieceIndex
Hwo.prototype.switchesBeforePiece = function (pieceIndex) {
    var pi = 0;
    var num = 0;
    for (var p = 0; p < this.track.pieces.length; p++) {
        pi = this.track.getPieceIndexByOffset(this.cars.car[0].pieceIndex[0], p);
        if (this.track.pieces[pi].switch) {
            num++;
        }
        if (pi == pieceIndex) {
            break;
        }
    }
    return num;
}



Hwo.prototype.calculateLapMaxSlip = function () {
    if (Math.abs(this.maxSlipAngle) < Math.abs(this.cars.car[0].angle[0]) && !this.hasRammed && !this.wasRammed) {
        this.maxSlipAngle = this.cars.car[0].angle[0];
    }
}





Hwo.prototype.calculatePieceMaxSpeed = function () {
   if (this.cars.car[0].speed[0] < this.track.pieces[this.cars.car[0].pieceIndex[0]].maxSpeed && !this.hasRammed && !this.wasRammed) {
        this.track.pieces[this.cars.car[0].pieceIndex[0]].maxSpeed = this.cars.car[0].speed[0];
    }
}



Hwo.prototype.calculatePieceMaxSlip = function () {
    if (Math.abs(this.track.pieces[this.cars.car[0].pieceIndex[0]].maxSlipAngle) < Math.abs(this.cars.car[0].angle[0]) && !this.hasRammed && !this.wasRammed) {
        this.track.pieces[this.cars.car[0].pieceIndex[0]].maxSlipAngle = this.cars.car[0].angle[0];
    }
}


Hwo.prototype.updateAngleParameters = function() {
    if ( !this.cars.car[0].wasRammed && !this.cars.car[0].hasRammed && this.cars.car[0].lap[0] > 0) {
        debug("Looks like I can't drive. ");
        debug("Adjusted safe angle: "+this.safeAngle+">"+(this.safeAngle -= 5))
    }
}

Hwo.prototype.updateSafeSpeeds = function(reason) {
    if (reason === "crash" && !this.cars.car[0].wasRammed && !this.cars.car[0].hasRammed){

        // Adjust global traction and update safe speeds. If crashed more than x times during first lap.
        if (this.cars.car[0].lap[0] <= 1 && this.cars.car[0].lapCrashes++ > 2) {
            this.track.baseFriction -= 0.002;
            for (var p = 0; p < this.track.pieces.length; p++) {
                for (var l = 0; l < this.track.lanes.length; l++) {
                    this.track.pieces[p].baseLaneSafeSpeed[l] = Math.sqrt( 9.8 * this.track.baseFriction * this.track.pieces[p].laneRadius[l]  );
                    this.track.pieces[p].laneSafeSpeed[l] = this.track.pieces[p].baseLaneSafeSpeed[l] * this.track.pieces[p].laneSafeSpeedModifier[l];
                }
            }
            debug("Crashed "+this.cars.car[0].lapCrashes+" times. Too slippery!");
        }

        // Adjust safe speed for piece where crash happened.
        else {
            var p = this.cars.car[0].pieceIndex[0];
            var p1 = this.cars.car[0].pieceIndex[1];
            for (var l = 0; l < this.track.lanes.length; l++) {
                this.track.pieces[p].laneSafeSpeedModifier[l] *= 0.8;
                this.track.pieces[p].baseLaneSafeSpeed[l] = Math.sqrt( 9.8 * this.track.baseFriction * this.track.pieces[p].laneRadius[l]  );
                this.track.pieces[p].laneSafeSpeed[l] = this.track.pieces[p].baseLaneSafeSpeed[l] * this.track.pieces[p].laneSafeSpeedModifier[l];

                this.track.pieces[p1].laneSafeSpeedModifier[l] *= 0.8;
                this.track.pieces[p1].baseLaneSafeSpeed[l] = Math.sqrt( 9.8 * this.track.baseFriction * this.track.pieces[p1].laneRadius[l]  );
                this.track.pieces[p1].laneSafeSpeed[l] = this.track.pieces[p1].baseLaneSafeSpeed[l] * this.track.pieces[p1].laneSafeSpeedModifier[l];
            }
            debug("That piece was slippery!");
        }
    }

    // on lap finish update all pieces based on max slip angle
    else {
        // No crashes, bump global traction
        if (Math.abs(this.maxSlipAngle) < 40 && this.cars.car[0].lapCrashes < 2 && this.cars.car[0].lap[0] <= 1 ) {
            this.track.baseFriction += 0.002;
            debug("No crashes. New baseFriction=" + this.track.baseFriction);
        }


        // Two pieces after longest straight. To fix turbo crashing.
        var c1 = this.track.getPieceIndexByOffset(this.track.maxStraightEnd, 1);
        var c2 = this.track.getPieceIndexByOffset(this.track.maxStraightEnd, 2);

        // Adjust piece local safe speed
        for (var p = 0; p < this.track.pieces.length; p++) {
            var p1 = (p>0)?p-1:this.track.pieces.length - 1;
            var p2 = (p>1)?p-2:this.track.pieces.length - 2;

                for (var l = 0; l < this.track.lanes.length; l++) {

                    if (Math.abs(this.track.pieces[p].maxSlipAngle) >= 50) {
                        this.track.pieces[p].laneSafeSpeedModifier[l] *= 1;
                        this.track.pieces[p1].laneSafeSpeedModifier[l] *= 0.90;
                        this.track.pieces[p2].laneSafeSpeedModifier[l] *= 0.95;
                    }

                    // don't update turbo corners
                    if (p !== c1 && p !== c2) {
                        if (Math.abs(this.track.pieces[p].maxSlipAngle) < 20 && this.track.pieces[p].baseRadius >60) {
                            this.track.pieces[p].laneSafeSpeedModifier[l] *= 1.05;
                            this.track.pieces[p1].laneSafeSpeedModifier[l] *= 1.05;
                        }
                        if (Math.abs(this.track.pieces[p].maxSlipAngle) < 20 && this.track.pieces[p].baseRadius <=60) {
                            this.track.pieces[p].laneSafeSpeedModifier[l] *= 1.02;
                            this.track.pieces[p1].laneSafeSpeedModifier[l] *= 1.02;
                        }
                        if (Math.abs(this.track.pieces[p].maxSlipAngle) < 30 && Math.abs(this.track.pieces[p].maxSlipAngle) >= 20  && this.track.pieces[p].baseRadius >60) {
                            this.track.pieces[p].laneSafeSpeedModifier[l] *= 1.04;
                            this.track.pieces[p1].laneSafeSpeedModifier[l] *= 1.04;
                        }
                        if (Math.abs(this.track.pieces[p].maxSlipAngle) < 30 && Math.abs(this.track.pieces[p].maxSlipAngle) >= 20  && this.track.pieces[p].baseRadius <=60) {
                            this.track.pieces[p].laneSafeSpeedModifier[l] *= 1.01;
                            this.track.pieces[p1].laneSafeSpeedModifier[l] *= 1.01;
                        }
                        if (Math.abs(this.track.pieces[p].maxSlipAngle) < 45 && Math.abs(this.track.pieces[p].maxSlipAngle) >= 30) {
                            this.track.pieces[p].laneSafeSpeedModifier[l] *= 1.005;
                            this.track.pieces[p1].laneSafeSpeedModifier[l] *= 1.005;
                        }
                    }

                    this.track.pieces[p].baseLaneSafeSpeed[l] = Math.sqrt(9.8 * this.track.baseFriction * this.track.pieces[p].laneRadius[l])
                    this.track.pieces[p].laneSafeSpeed[l] = this.track.pieces[p].baseLaneSafeSpeed[l] * this.track.pieces[p].laneSafeSpeedModifier[l];

                }

            this.track.pieces[p].maxSpeed = 0;
            this.track.pieces[p].maxSlipAngle = 0;

            debug("Adjusted piece "+p+" safe speed with "+this.track.pieces[p].laneSafeSpeedModifier[0])
        }
    }
}




Hwo.prototype.whoa = function () {
    var currentPiece = this.cars.car[0].pieceIndex[0];
    var distanceToPiece = this.track.pieces[currentPiece].laneLength[this.cars.car[0].endLaneIndex[0]] - this.cars.car[0].inPieceDistance[0];
    var ohshit = false;

    for (var p = 1; p <= 7; p++) {
        var pi = (currentPiece + p)%this.track.pieces.length;
        if (p>1) {
            distanceToPiece = distanceToPiece + this.track.pieces[pi].laneLength[this.cars.car[0].endLaneIndex[0]];
        }


        var safeSpeed = this.track.pieces[pi].laneSafeSpeed[this.cars.car[0].endLaneIndex[0]];
        //debug("Safe speed: "+safeSpeed);
        var ticksToSafeSpeed = this.cars.car[0].predictTimeToSpeed(safeSpeed, this.cars.car[0].speed[0], 0.0);
        var distanceToSafeSpeed = this.cars.car[0].predictDistanceAtThrottle(0.0, this.cars.car[0].speed[0], ticksToSafeSpeed+3);

        if (distanceToSafeSpeed >= distanceToPiece) {
            ohshit = true;
            break;
        }
    }

    return ohshit;
}





Hwo.prototype.inCurve = function() {
    return this.track.pieces[this.cars.car[0].pieceIndex[0]].baseRadius < Infinity;
}




Hwo.prototype.curveTightens = function() {
    var currentPiece = this.cars.car[0].pieceIndex[0];
    var currentPieceRadius = this.track.pieces[currentPiece].baseRadius;
    var offsetPieceIndex;
    var offsetPieceRadius;
    var curveTightens = false;
    var s = 0; // straights before corner
    for (var p = 1; p <= 4; p++) {
        offsetPieceIndex = this.track.getPieceIndexByOffset(currentPiece, p);
        offsetPieceRadius = this.track.pieces[offsetPieceIndex].baseRadius;
        if (offsetPieceRadius === Infinity) {
            s++;
        }
        if (s > 1) { break; }
        if (offsetPieceRadius !== Infinity && currentPieceRadius !== Infinity && (offsetPieceRadius < currentPieceRadius || offsetPieceRadius < 60)) {
            curveTightens = true;
            break;
        }
    }
    return curveTightens;
}





Hwo.prototype.decideThrottle = function(gameTick) {
    // Full throttle by default
    this.cars.car[0].throttle = 1;

    if (this.willCrash) {
        this.cars.car[0].throttle = 0.0;
    }
    else {
        this.checkIfBehindSomeone();
    }
    return;


    if (/*!this.inCurve() && */this.whoa()) {
        this.cars.car[0].throttle = 0;
        debug("Whoa!", "red");
    }
    /*
    else if (this.inCurve() && !this.curveTightens()) {
    */
    else if (
        Math.abs(this.cars.car[0].angle[0]) > 25 && this.cars.car[0].angularSpeed[0] * sign(this.cars.car[0].angle[0]) > 2.5
        ||
        Math.abs(this.cars.car[0].angle[0]) > 37 && this.cars.car[0].angularSpeed[0] * sign(this.cars.car[0].angle[0]) > 1.8
        ||
        Math.abs(this.cars.car[0].angle[0]) > 40 && this.cars.car[0].angularSpeed[0] * sign(this.cars.car[0].angle[0]) > 0.5
        ||
        Math.abs(this.cars.car[0].angle[0]) > 50 && this.cars.car[0].angularSpeed[0] * sign(this.cars.car[0].angle[0]) > 0.1
        ||
        Math.abs(this.cars.car[0].angle[0]) > 55 && this.cars.car[0].angularSpeed[0] * sign(this.cars.car[0].angle[0]) > 0.05) {

        this.cars.car[0].throttle = 0.0;
        debug("Watch the angle!", "red");
    }
    else if (Math.abs(this.cars.car[0].angularAcceleration[0]) > 0.50) {
        debug("Too much G!", "red");
        this.cars.car[0].throttle = 0;
    }
    else {
        this.cars.car[0].throttle = 1.0;
    }
    if (this.cars.car[0].throttle == 1.0 && !this.cars.car[0].isCrashed) {
        //debug("GO!", "green");
    }
}



Hwo.prototype.gonnaCrash = function () {
    var lookAhead = 150;

    var prediction = {};

    for (var n = 0; n <= lookAhead; n++) {
        var result = false;
        //throttle = (n>5)? 0.0 : 1.0;
        throttle = (n>2)? 0.0 : 1.0;
        this.cars.car[0].predictAngleWorker(this.track, prediction, throttle, n);

        if (Math.abs(prediction.angle) >= this.safeAngle)
        {
            result = true;
            break;
        }
    }
    return result;
}


Hwo.prototype.checkIfBehindSomeone = function () {
    this.stuckBehindSomeone = false;
    this.lanesTaken = [];
    if (this.cars.car[0].isCrashed) {return;}
    for (var c = 1; c < this.cars.car.length; c++) {
        if (this.cars.car[c].endLaneIndex[0] === this.cars.car[0].endLaneIndex[0]) {
            var distance = Infinity;
            if (this.cars.car[0].pieceIndex[0] === this.cars.car[c].pieceIndex[0] && !this.cars.car[c].isCrashed) {
                distance = this.cars.car[0].inPieceDistance[0] - this.cars.car[0].inPieceDistance[0];
            }
            if (this.track.getPieceIndexByOffset(this.cars.car[0].pieceIndex[0], 1) === this.cars.car[c].pieceIndex[0]  && !this.cars.car[c].isCrashed) {
                distance = this.track.pieces[this.cars.car[0].pieceIndex[0]].laneLength[this.cars.car[0].endLaneIndex[0]] - this.cars.car[0].inPieceDistance[0] + this.cars.car[c].inPieceDistance[0];
            }
debug(distance);
            if (distance < 50) {
                this.stuckBehindSomeone = true;
                this.lanesTaken.push(this.cars.car[c].endLaneIndex[0]);
                debug("Slow poke in front");
            }
        }
        else {
            var distance = Infinity;
            if (this.cars.car[0].pieceIndex[0] === this.cars.car[c].pieceIndex[0] && !this.cars.car[c].isCrashed) {
                distance = this.cars.car[0].inPieceDistance[0] - this.cars.car[0].inPieceDistance[0];
            }
            if (this.track.getPieceIndexByOffset(this.cars.car[0].pieceIndex[0], 1) === this.cars.car[c].pieceIndex[0]  && !this.cars.car[c].isCrashed) {
                distance = this.track.pieces[this.cars.car[0].pieceIndex[0]].laneLength[this.cars.car[0].endLaneIndex[0]] - this.cars.car[0].inPieceDistance[0] + this.cars.car[c].inPieceDistance[0];
            }

            if (distance < 50) {
                this.lanesTaken.push (this.cars.car[c].endLaneIndex[0]);
                debug("Lane "+this.cars.car[c].endLaneIndex[0]+" is occupied.");
            }
        }
    }
}
module.exports = Hwo;