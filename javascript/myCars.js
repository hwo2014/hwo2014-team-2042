/**
 * Initialize cars array and
 * push own car to index 0
 * @param carsData {object} JSON object containing cars' data
 */

Cars = function (myCar) {
    this.positionsInitialized = false;
    this.ownCarColor = myCar.color;
    this.accelerationDataOk = false;
}


Cars.prototype.init = function (carsData) {
    this.colorToIndex = [];
    this.car = [];

    var offset = 1;

    for (var c = 0; c < carsData.length; c++) {
        var car = new Car(carsData[c]);
        if (car.color == this.ownCarColor) {
            this.car.unshift(car);
            this.colorToIndex[car.color] = 0;
            offset = 0;
        }
        else {
            this.car.push(car);
            this.colorToIndex[car.color] = c + offset;
        }
    }
}


/**
 * Initialize car position data on first carPositions message
 * @param carPositions {object} Cars positions data.
 */
Cars.prototype.initData = function (carPositions) {
    for (var p = 0; p < carPositions.length; p++) {
        var i = this.colorToIndex[carPositions[p].id.color];
        this.car[i].initData(carPositions[p]);
    }
    this.positionsInitialized=true;
}




/**
 * Update car data from carPositions message for all cars
 * @param carPositions {object} JSON object containing position data for cars
 */
Cars.prototype.updatePositions = function (carPositions) {
    for (var p = 0; p < carPositions.length; p++) {
        var i = this.colorToIndex[carPositions[p].id.color];
        this.car[i].updatePosition(carPositions[p]);
    }
}






/**
 * Calculates data for cars
 */
Cars.prototype.calculateData = function (track) {
    for (var c = 0; c < this.car.length; c++) {
        this.car[c].calculateData(track);
    }
}








// ============================================================================================================
// ============================================================================================================
// ============================================================================================================




/**
 * Car constructor
 * @param carData {object} JSON object containing car data
 * @constructor
 */
Car = function (carData) {
    this.name = carData.id.name;
    this.color = carData.id.color;
    this.length = carData.dimensions.length;
    this.width = carData.dimensions.width;
    this.flagPosition = carData.dimensions.guideFlagPosition;

    // position
    this.inPieceDistance = [];
    this.angle = [];
    this.pieceIndex = [];
    this.startLaneIndex = [];
    this.endLaneIndex = [];
    this.lap = [];

    // speed and forces
    this.speed = [];
    this.centripetalForce = [];
    this.angularAcceleration = [];
    this.angularSpeed = [];


    // throttle
    this.throttle = 1.0;

    // model stuff
    this.initialAcceleration = 0.2;
    this.dragMultiplier = 0.98;
    this.accelerationDataOk = false;

    this.angleDataOk = false;
    this.angleDataFailed = false;
    this.angleDataFailedDelay = true;
    this.angleDataFailedDelayTicks = 0;
    this.angularAcceleration = [];
    this.angularSpeed = [];
    this.angleParamA = 0.530330085881027;
    this.angleParamB = 0.10000000000707321;
    this.angleParamC = 0.0012500000006921592;
    this.angleParamD = 0.29999999999495347;
    this.pieceRadius = [];
    this.pieceAngle = [];

    this.turboActive = false;
    this.turboAvailable = false;
    this.turboFactor = 1;
    this.turboMultiplier = 1;

    this.wasRammed = false;
    this.wasRammedTicks = 0;
    this.hasRammed = false;
    this.hasRammedTicks = 0

    // other
    this.isCrashed = false;
    this.ticksSinceCrash = 0;
    this.lapCrashes = 0;
    this.maxSpeed = 0;
}







/**
 * Updates single car data from server message
 * @param carPosition {object} Car position data as object.
 */
Car.prototype.updatePosition = function (carPosition) {
    this.bufferData();

    this.inPieceDistance[0] = carPosition.piecePosition.inPieceDistance;
    this.angle[0] = carPosition.angle;
    this.pieceIndex[0] = carPosition.piecePosition.pieceIndex;
    this.startLaneIndex[0] = carPosition.piecePosition.lane.startLaneIndex;
    this.endLaneIndex[0] = carPosition.piecePosition.lane.endLaneIndex;
    this.lap[0] = carPosition.piecePosition.lap;
}





/**
 * Init single car data from first carPositions server message
 * @param carPosition {object} Car position data as object.
 */
Car.prototype.initData = function (carPosition) {
    this.inPieceDistance[1] = this.inPieceDistance[0] = carPosition.piecePosition.inPieceDistance;
    this.angle[1] = this.angle[0] = carPosition.angle;
    this.pieceIndex[1] = this.pieceIndex[0] = carPosition.piecePosition.pieceIndex;
    this.startLaneIndex[1] = this.startLaneIndex[0] = carPosition.piecePosition.lane.startLaneIndex;
    this.endLaneIndex[1] = this.endLaneIndex[0] = carPosition.piecePosition.lane.endLaneIndex;
    this.lap[1] = this.lap[0] = carPosition.piecePosition.lap;

    // speed and forces
    this.speed[1] = this.speed[0] = 0;
    this.centripetalForce[1] = this.centripetalForce[0] = 0;
    this.angularAcceleration[1] = this.angularAcceleration[0] = 0;
    this.angularSpeed[1] = this.angularSpeed[0] = 0;

}






Car.prototype.bufferData = function() {
    keepNLastValues(this.inPieceDistance, 5);
    keepNLastValues(this.angle, 5);
    keepNLastValues(this.pieceIndex, 1);
    keepNLastValues(this.startLaneIndex, 1);
    keepNLastValues(this.endLaneIndex, 1);
    keepNLastValues(this.lap, 1);
    keepNLastValues(this.speed, 5);
    keepNLastValues(this.centripetalForce, 5);
    keepNLastValues(this.angularAcceleration, 5);
    keepNLastValues(this.angularSpeed, 5);
    keepNLastValues(this.pieceRadius, 5);
    keepNLastValues(this.pieceAngle, 5);
}




/**
 * Calculates speed and other car parameters
 */
Car.prototype.calculateData = function (track) {

    var pieceIndex = (this.pieceIndex[0] >= 0)? this.pieceIndex[0] : track.pieces.length - this.pieceIndex[0];
    this.getSpeed(track);
    this.angularSpeed[0] = this.angle[0] - this.angle[1];
    this.angularAcceleration[0] = this.angularSpeed[0] - this.angularSpeed[1];
    this.centripetalForce[0] = this.getCentripetalForce(this.speed[0], pieceIndex, track);
    this.pieceRadius[0] = track.pieces[pieceIndex].laneRadius[this.startLaneIndex[0]];
    this.pieceAngle[0] = track.pieces[pieceIndex].angle;

    this.turboMultiplier = (this.turboActive)? this.turboFactor : 1;

    this.averageSpeed = (this.speed[0] + this.speed[1]  + this.speed[2] + this.speed[3] + this.speed[4]) / 5;
    if (this.isCrashed) {this.ticksSinceCrash = 0}
    else {this.ticksSinceCrash++}


    if ((this.speed[0] - this.speed[1]) > this.initialAcceleration * this.turboMultiplier * 2 && this.accelerationDataOk) {
        this.wasRammed = true;
        this.wasRammedTicks = 0;
    }
    if (this.wasRammed) {
        if (this.wasRammedTicks++ > 5) {
            this.wasRammed = false;
        }
    }
    if (!this.wasRammed && ((this.speed[1] - this.speed[0]) > this.initialAcceleration * this.turboMultiplier * 2)  && this.accelerationDataOk ) {
        this.hasRammed = true;
        this.hasRammedTicks = 0;
    }
    if (this.hasRammed) {
        if (this.hasRammedTicks++ > 5) {
            this.hasRammed = false;
        }
    }
}







/**
 * Calculates car speed from position
 */
Car.prototype.getSpeed = function (track) {
    if (this.pieceIndex[1] == this.pieceIndex[0]) {
        this.speed[0] = this.inPieceDistance[0] - this.inPieceDistance[1];
    }
    if (this.pieceIndex[1] !== this.pieceIndex[0] && !track.pieces[this.pieceIndex[1]].switch) {
        this.speed[0] = this.inPieceDistance[0] + track.pieces[this.pieceIndex[1]].laneLength[this.startLaneIndex[0]] - this.inPieceDistance[1];
    }
    // if i can't figure out switch physics and lane length.
    if (this.pieceIndex[1] !== this.pieceIndex[0] && track.pieces[this.pieceIndex[1]].switch) {
        this.speed[0] = this.speed[1] * 1.00;
    }
}







// ***************************************
// Calculates car centripetal force
// ***************************************
Car.prototype.getCentripetalForce = function (speed, pieceIndex, track) {
    if (typeof pieceIndex === "undefined") {pieceIndex = this.pieceIndex[0]}
    if (typeof speed === "undefined") {speed = this.speed[0]}

    var centripetalForce = this.speed[0] * this.speed[0] / track.pieces[pieceIndex].laneRadius[this.endLaneIndex[0]];
    return centripetalForce;
}







// ***************************************
// Try to predict angular acceleration
// ***************************************
Car.prototype.predictAngle = function (track, throttle, ticks) {
    var prediction = {};
    for (var n = 0; n <= ticks; n++) {
        this.predictAngleWorker(track, prediction, throttle, n);
    }
    //return Math.abs(prediction.maxAngle);
    return prediction;
}

Car.prototype.predictAngleWorker = function (track, prediction, throttle, n) {
    prediction.throttle = throttle;

    if (n == 0) {
        prediction.last = {};

        prediction.angle = this.angle[0];
        prediction.speed = this.speed[0];
        prediction.angularSpeed = this.angularSpeed[0];
        prediction.angularAcceleration = this.angularAcceleration[0];

        prediction.inPieceDistance = this.inPieceDistance[0];
        prediction.pieceIndex = this.pieceIndex[0];
        //debug("Started with piece: " + prediction.pieceIndex)

        prediction.startLaneIndex = this.startLaneIndex[0];
        prediction.endLaneIndex = this.endLaneIndex[0];
    }
    else {
        prediction.angularAcceleration = prediction.last.aForce - prediction.last.bForce - prediction.last.cForce;
        prediction.angularSpeed = prediction.angularAcceleration + prediction.angularSpeed;
        prediction.angle = prediction.angularSpeed + prediction.angle;

        prediction.speed = this.predictSpeed(prediction.last.speed, prediction.throttle, 1);

        prediction.inPieceDistance = prediction.last.inPieceDistance + prediction.speed;
    }

    if (prediction.inPieceDistance > track.pieces[prediction.pieceIndex].laneLength[prediction.endLaneIndex]) {
        prediction.inPieceDistance = prediction.inPieceDistance - track.pieces[prediction.pieceIndex].laneLength[prediction.endLaneIndex];
        prediction.pieceIndex = track.getPieceIndexByOffset (prediction.pieceIndex, 1);
    }

    prediction.pieceIsSwitch = track.pieces[prediction.pieceIndex].switch && track.pieces[prediction.pieceIndex].baseRadius >= 200;
    if (prediction.pieceIsSwitch && (prediction.startLaneIndex === prediction.endLaneIndex) && this.angleDataOk && hwo.doSwitches) {
        var shortestLane = this.shortestLaneToNextSwitchPrediction(prediction, track);
        prediction.endLaneIndex = shortestLane;
    }
    if (!prediction.pieceIsSwitch) {
        prediction.startLaneIndex = prediction.endLaneIndex;
    }

    //debug("Piece: " + prediction.pieceIndex + " " + prediction.startLaneIndex + ">" + prediction.endLaneIndex,"magenta");


    prediction.laneRadius = track.pieces[prediction.pieceIndex].laneRadius[prediction.endLaneIndex];
    prediction.pieceAngle = track.pieces[prediction.pieceIndex].angle;


    // special handling for switches
    var safety = (prediction.pieceIsSwitch && ( prediction.endLaneIndex !== prediction.startLaneIndex ) )? 1.05: 1;
    //var safety = 1;
        // special handling for suzuka 6th piece switch


    prediction.aForce = (this.angleParamA * safety * prediction.speed * prediction.speed / Math.sqrt(prediction.laneRadius) - this.angleParamD * prediction.speed);
    prediction.aForce = Math.max(prediction.aForce, 0) * sign(prediction.pieceAngle); // Limit to 0..X
    prediction.bForce = this.angleParamB * prediction.angularSpeed;
    prediction.cForce = this.angleParamC * prediction.angle * prediction.speed;


    prediction.maxAngle = (Math.abs(prediction.angle) > Math.abs(prediction.maxAngle))? prediction.angle : prediction.maxAngle;
    //debug("PR MAX A: "+prediction.angle);


    // Save state
    prediction.last = JSON.parse(JSON.stringify(prediction));
    if (typeof prediction.last.last !== "undefined") {delete prediction.last.last;} // pevents recurent object
}



Car.prototype.shortestLaneToNextSwitchPrediction = function(prediction, track) {
    var laneLeft = Math.max(0, prediction.endLaneIndex-1);
    var laneRight = Math.min(track.lanes.length - 1, prediction.endLaneIndex+1);
    var shortestLaneLength = 0;
    var shortestLane = prediction.endLaneIndex;


    for (var l = laneLeft; l <= laneRight; l++) {
        var s = 0;
        var laneLength = 0;
        for (var p = 0; p < track.pieces.length; p++) {
            pi = track.getPieceIndexByOffset(prediction.pieceIndex, p);
            if (track.pieces[pi].switch && track.pieces[pi].baseRadius >= 200) {
                s++
            }
            if (s > 1) {
                break;
            }
            laneLength += track.pieces[pi].laneLength[l];
        }
        if (shortestLaneLength == 0 || laneLength < shortestLaneLength) {
            shortestLaneLength = laneLength;
            shortestLane = l;
        }
    }
    return shortestLane;
}



// ***************************************
// Calculates multipliers for speed prediction
// ***************************************
Car.prototype.calculateAngleMultipliers = function () {
    if (
        // check all angles in case of crash or other unforeseen circumstances
        Math.abs(this.angle[1]) > 0
            && Math.abs(this.angle[2]) > 0
            && Math.abs(this.angle[3]) > 0
            && Math.abs(this.angle[4]) > 0
            // same radius corner. just in case.
            && this.pieceRadius[1] !== Infinity
            && this.pieceRadius[2] !== Infinity
            && this.pieceRadius[3] !== Infinity
            && this.pieceRadius[4] !== Infinity
            // same direction corner
            && sign(this.pieceAngle[1]) === sign(this.pieceAngle[2])
            && sign(this.pieceAngle[2]) === sign(this.pieceAngle[3])
            && sign(this.pieceAngle[3]) === sign(this.pieceAngle[4])
            // not bumped?
            && !this.hasRammed
            && !this.wasRammed

        ) {
        var V3 = this.speed[1];
        var V2 = this.speed[2];
        var V1 = this.speed[3];
        var V0 = this.speed[4];

        var AA4 = this.angularAcceleration[0];
        var AA3 = this.angularAcceleration[1];
        var AA2 = this.angularAcceleration[2];
        var AA1 = this.angularAcceleration[3];

        var AS3 = this.angularSpeed[1];
        var AS2 = this.angularSpeed[2];
        var AS1 = this.angularSpeed[3];
        var AS0 = this.angularSpeed[4];

        var A3 = this.angle[1];
        var A2 = this.angle[2];
        var A1 = this.angle[3];
        var A0 = this.angle[4];

        var R3 = this.pieceRadius[1];
        var R2 = this.pieceRadius[2];
        var R1 = this.pieceRadius[3];
        var R0 = this.pieceRadius[4];

        var PA3 = this.pieceAngle[1];
        var PA2 = this.pieceAngle[2];
        var PA1 = this.pieceAngle[3];
        var PA0 = this.pieceAngle[4];

        var aForce0 = Math.pow(V0, 2) / Math.sqrt(R0) * sign(PA0);
        var aForce1 = Math.pow(V1, 2) / Math.sqrt(R1) * sign(PA1);
        var aForce2 = Math.pow(V2, 2) / Math.sqrt(R2) * sign(PA2);
        var aForce3 = Math.pow(V3, 2) / Math.sqrt(R3) * sign(PA3);

        var dForce0 = V0;
        var dForce1 = V1;
        var dForce2 = V2;
        var dForce3 = V3;

        var bForce0 = AS0;
        var bForce1 = AS1;
        var bForce2 = AS2;
        var bForce3 = AS3;

        var cForce0 = V0 * A0;
        var cForce1 = V1 * A1;
        var cForce2 = V2 * A2;
        var cForce3 = V3 * A3;

        var a1 = [0, aForce0, dForce0, bForce0, cForce0, AA1];
        var a2 = [0, aForce1, dForce1, bForce1, cForce1, AA2];
        var a3 = [0, aForce2, dForce2, bForce2, cForce2, AA3];
        var a4 = [0, aForce3, dForce3, bForce3, cForce3, AA4];


        var t1 = a1;
        var t2 = a2;
        var t3 = a3;
        var t4 = a4;


// First we need a one in the first spot
        if (a1[1] == 0) {
            if (a2[1] != 0) {
                for (var i = 1; i <= 5; i++) {
                    var temp = a2[i]
                    a2[i] = a1[i]
                    a1[i] = temp
                }
            }
            if (a1[1] == 0) {
                for (var i = 1; i <= 5; i++) {
                    var temp = a3[i]
                    a3[i] = a1[i]
                    a1[i] = temp
                }
            }
            if (a1[1] == 0) {
                for (var i = 1; i <= 5; i++) {
                    var temp = a4[i]
                    a4[i] = a1[i]
                    a1[i] = temp
                }
            }
        }
        if (a1[1] == 0) {
            debug("Slip calculation failed".red)
            debug("There is a 0 in the entire first row (no variables involve x)!")
            this.angleDataFailed = true;
        }
        if (a1[1] != 0) {
            var temp = a1[1]
            for (var i = 1; i <= 5; i++) {
                a1[i] = a1[i] / temp
            }
        }
// Now we should have a 1 in the first entry - Now to zero out the column
        var temp = -a2[1]

        for (var i = 1; i <= 5; i++) {
            a2[i] = a2[i] + (a1[i] * temp)
        }

        var temp = -a3[1]
        for (var i = 1; i <= 5; i++) {
            a3[i] = a3[i] + (a1[i] * temp)
        }

        var temp = -a4[1]
        for (var i = 1; i <= 5; i++) {
            a4[i] = a4[i] + (a1[i] * temp)
        }

// Next Column  Check if 0 - if not put a 1 there
        if (a2[2] == 0) {   		// if = to 0 switch rows
            for (var i = 2; i <= 5; i++) {
                var temp = a2[i]
                a2[i] = a3[i]
                a3[i] = temp
            }
            if (a2[2] == 0) {
                for (var i = 2; i <= 5; i++) {
                    var temp = a2[i]
                    a2[i] = a4[i]
                    a4[i] = temp
                }
            }
        }
        if (a2[2] == 0) {   		// if = to 0 switch rows
            this.angleDataFailed = true;
            debug("Slip calculation failed".red)
            debug("System has infinitely or No solutions!")
        }
        if (a2[2] != 0) {
            var temp = a2[2]
            a2[2] = a2[2] / temp		// for statement would have taken longer
            a2[3] = a2[3] / temp
            a2[4] = a2[4] / temp
            a2[5] = a2[5] / temp

// zero out columns below
            var temp = -a3[2]
            for (var i = 2; i <= 5; i++) {
                a3[i] = a3[i] + (a2[i] * temp)
            }
            var temp = -a4[2]

            for (var i = 2; i <= 5; i++) {
                a4[i] = a4[i] + (a2[i] * temp)
            }
// zero out column above
            var temp = -a1[2]
            for (var i = 2; i <= 5; i++) {
                a1[i] = a1[i] + (a2[i] * temp)
            }
        }   // ends if != 0

        // Next Column  Check if 0 - if not put a 1 there
        if (a3[3] == 0) {   		// if = to 0 switch rows
            for (var i = 2; i <= 5; i++) {
                var temp = a3[i]
                a3[i] = a4[i]
                a4[i] = temp
            }
        }
        if (a3[3] == 0) {   		// if = to 0 switch rows
            this.angleDataFailed = true;
            debug("Slip calculation failed".red)
            debug("System has infinitely or No solutions! (3rd pivot)")
        }
        if (a3[3] != 0) {
            var temp = a3[3]
            a3[3] = a3[3] / temp		// for statement would have taken longer
            a3[4] = a3[4] / temp
            a3[5] = a3[5] / temp

// zero out columns below
            var temp = -a4[3]
            for (var i = 3; i <= 5; i++) {
                a4[i] = a4[i] + (a3[i] * temp)
            }
// zero out column above
            var temp = -a1[3]
            for (var i = 3; i <= 5; i++) {
                a1[i] = a1[i] + (a3[i] * temp)
            }
            var temp = -a2[3]
            for (var i = 3; i <= 5; i++) {
                a2[i] = a2[i] + (a3[i] * temp)
            }
        }   // ends if != 0

// Final Column

        if (a4[4] != 0) {
            var temp = a4[4]
            a4[4] = a4[4] / temp		// for statement would have taken longer
            a4[5] = a4[5] / temp
        }

        if (a4[4] == 0) {   		// if = to 0 switch rows
            this.angleDataFailed = true;
            debug("Slip calculation failed".red)
            debug("System has infinitely or No solutions!")
        }
// zero out column above
        var temp = -a3[4]

        a3[4] = a3[4] + (a4[4] * temp)
        a3[5] = a3[5] + (a4[5] * temp)

        var temp = -a2[4]
        a2[4] = a2[4] + (a4[4] * temp)
        a2[5] = a2[5] + (a4[5] * temp)

        var temp = -a1[4]
        a1[4] = a1[4] + (a4[4] * temp)
        a1[5] = a1[5] + (a4[5] * temp)


        if (
            Math.abs(a1[5]) > 1.0 || Math.abs(a1[5]) < 0.2
        ||  Math.abs(a2[5]) > 0.5 || Math.abs(a2[5]) < 0.2
        ||  Math.abs(a3[5]) > 0.3 || Math.abs(a3[5]) < 0.01
        ||  Math.abs(a4[5]) > 0.1 || Math.abs(a4[5]) < 0
            )
        {
            this.angleDataFailed = true;
            debug("Slip calculation failed".red)
            debug("Parameters don't seem right")
        }

        if (!this.angleDataFailed) {
            this.angleParamA = Math.abs(a1[5]);
            this.angleParamD = Math.abs(a2[5]);
            this.angleParamB = Math.abs(a3[5]);
            this.angleParamC = Math.abs(a4[5]);


            debug("A: ".green + this.angleParamA);
            debug("B: ".green + this.angleParamB);
            debug("C: ".green + this.angleParamC);
            debug("D: ".green + this.angleParamD);


            this.angleDataOk = true;
        }
   }


    // retry angle parameters calculation
    if (this.angleDataFailed && (this.pieceIndex[0] !== this.pieceIndex[1]) && this.pieceAngle !== 0.0) {
        this.angleDataFailedDelay = true;
    }
    if (this.angleDataFailed && this.angleDataFailedDelay) {
        this.angleDataFailedDelayTicks += 1;
    }
    if (this.angleDataFailed && this.angleDataFailedDelayTicks >=5){
        this.angleDataFailedDelayTicks = 0;
        this.angleDataFailed = 0;
    }

}







// ***************************************
// Calculates multipliers for speed prediction
// ***************************************
Car.prototype.calculateSpeedMultipliers = function() {
    if ( this.speed[2] > 0 && !this.accelerationDataOk) {
        this.initialAcceleration = this.speed[2]  / this.throttle;
        this.dragMultiplier = ( this.speed[1] / this.throttle - this.initialAcceleration ) / this.initialAcceleration;
        this.maxSpeed = this.predictSpeed(0, 1, 1000);
        this.accelerationDataOk = true;
        debug("Car data: acc="+this.initialAcceleration+" drag="+this.dragMultiplier);
    }
}







Car.prototype.predictSpeed = function (startSpeed, throttle, ticks) {
    if (typeof ticks === 'undefined') {ticks = 1}

    var speedPrediction = Math.pow(this.dragMultiplier, ticks) * startSpeed + this.initialAcceleration * this.turboMultiplier * throttle * (Math.pow(this.dragMultiplier, ticks) - 1.0) / (this.dragMultiplier - 1.0);
    return speedPrediction;
}







Car.prototype.predictTimeToSpeed = function (endSpeed, startSpeed, throttle) { // n = f(V, time, throttle)
    if (typeof throttle === 'undefined') {throttle = this.throttle}
    if (typeof startSpeed === 'undefined') {startSpeed = this.speed[0]}
    var tmp1 = endSpeed * (this.dragMultiplier - 1) + this.initialAcceleration * throttle * this.turboMultiplier;
    var tmp2 = startSpeed * (this.dragMultiplier - 1) + this.initialAcceleration * throttle * this.turboMultiplier;
    var tmp3 = log(this.dragMultiplier, tmp1 / tmp2);
    return tmp3;
}







Car.prototype.predictDistanceAtThrottle = function (throttle, startSpeed, ticks) { // Sn = f(V, time, throttle)
    if (typeof throttle === 'undefined') {throttle = this.throttle}
    if (typeof startSpeed === 'undefined') {startSpeed = this.speed[0]}
    if (typeof ticks === 'undefined') {ticks = 1}
    var tmp1 = (this.initialAcceleration*(Math.pow(this.dragMultiplier,1 + ticks) + ticks - this.dragMultiplier*(1 + ticks))*throttle*this.turboMultiplier + (-1 + this.dragMultiplier)*this.dragMultiplier*(-1 + Math.pow(this.dragMultiplier,ticks))*startSpeed)/Math.pow(-1 + this.dragMultiplier,2)
    return tmp1;
}





module.exports = Cars;