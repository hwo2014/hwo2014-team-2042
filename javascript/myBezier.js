/*

 var resolution = 0.0001;
 var factor = 0.15;



 for (factor = 0.1; factor < 0.2; factor += 0.0001) {
 var l1 = 0;
 var l2 = 0;

 var len = 100;
 var startPoint = {x:0.00            , y:-10.00}
 var ctrlPoint1 = {x:factor*len      , y:-10.00}
 var ctrlPoint2 = {x:len-factor*len  , y: 10.00}
 var endPoint   = {x:len             , y: 10.00}
 var prevPoint  = startPoint;
 for (t = 0; t < 1.0 + resolution; t += resolution) {
 var point = bezier(startPoint, ctrlPoint1, ctrlPoint2, endPoint, t)
 l1 += Math.sqrt(Math.pow((point.x - prevPoint.x), 2) + Math.pow((point.y - prevPoint.y), 2));
 prevPoint = point;
 }

 var len = 94;
 var startPoint = {x:0.00            , y:-10.00}
 var ctrlPoint1 = {x:factor*len      , y:-10.00}
 var ctrlPoint2 = {x:len-factor*len  , y: 10.00}
 var endPoint   = {x:len             , y: 10.00}
 var prevPoint  = startPoint;
 for (t = 0; t < 1.0 + resolution; t += resolution) {
 var point = bezier(startPoint, ctrlPoint1, ctrlPoint2, endPoint, t)
 l2 += Math.sqrt(Math.pow((point.x - prevPoint.x), 2) + Math.pow((point.y - prevPoint.y), 2));
 prevPoint = point;
 }

 debug(factor+": " +l1+" "+l2);
 }


 debug(l);
 exit(0);
 */



module.exports = bezier
module.exports.B1 = B1
module.exports.B2 = B2
module.exports.B3 = B3
module.exports.B4 = B4
module.exports.B4 = B4
module.exports.makeCrossing = makeCrossing

function B1(t) {
    return t * t * t
}

function B2(t) {
    return 3 * t * t * (1 - t)
}

function B3(t) {
    return 3 * t * (1 - t) * (1 - t)
}

function B4(t) {
    return (1 - t) * (1 - t) * (1 - t)
}

function bezier(start, control1, control2, end, t) {
    return {
        x: start.x * B4(t) + control1.x * B3(t) + control2.x * B2(t) + end.x * B1(t),
        y: start.y * B4(t) + control1.y * B3(t) + control2.y * B2(t) + end.y * B1(t)
    }
}

function makeCrossing(laneA, laneB) {
    var cubic = _.partial(bezier,
        laneA.startX, laneA.startY,
        laneA.midX, laneA.midY,
        laneB.midX, laneB.midY,
        laneB.endX, laneB.endY)

    var controlPoint1 = cubic(0.75)
    var controlPoint2 = cubic(0.25)

    debug(controlPoint1);
    debug(controlPoint2);
}