var net = require("net");
var JSONStream = require('JSONStream');

sprintf = require('./sprintf.min').sprintf
colors = require('./colors');
exit = require('./exit');


var Hwo = require('./myHwo');
hwo = new Hwo();







var tracks = [
    "keimola"
    ,"germany"
    ,"usa"
    ,"france"
    ,"elaeintarha"
    ,"imola"
    ,"england"
    ,"suzuka"
    ,"pentag"
    ,"keimola"
    ,"germany"
    ,"usa"
    ,"france"
    ,"elaeintarha"
    ,"imola"
    ,"england"
    ,"suzuka"
    ,"pentag"
    ,"keimola"
    ,"germany"
    ,"usa"
    ,"france"
    ,"elaeintarha"
    ,"imola"
    ,"england"
    ,"suzuka"
    ,"pentag"
];



if (hwo.environment === "TEST") {
    client = net.connect(hwo.serverPort, hwo.serverHost, function () {
        return hwo.send({
            msgType: "joinRace",
            data: {
                botId: {
                    name: hwo.botName
                    ,key: hwo.botKey
                }
                ,trackName: "pentag"//tracks[Math.floor(Math.random() * (tracks.length-1 - 0 + 1)) + 0]
                ,password: "zzz555"
                ,carCount: hwo.carCount
            }
        });
    });
}

else {
    client = net.connect(hwo.serverPort, hwo.serverHost, function () {
        return hwo.send({
            msgType: "join",
            data: {
                name: hwo.botName
                ,key: hwo.botKey
            }
        });
    });
}


if (hwo.environment === "TEST") {
    client.setTimeout(10000, function () {
        console.log("timeout");
        process.exit(0);
    });
}


jsonStream = client.pipe(JSONStream.parse());


jsonStream.on('data', function(data) {
    hwo.respondToMessage(data);
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});