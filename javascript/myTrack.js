var Track = function (trackData) {
    this.id = trackData.id;
    this.name = trackData.name;
    this.getLanesData(trackData.lanes);
    this.baseFriction = 0.048;
    this.fillPiecesData(trackData.pieces);

    this.maxStraightLen = 0;
    this.maxStraightStart = 0;
    this.maxStraightEnd = 0;
    this.getLongestStraight();
}

// ******************************************
// Fills missing and additional data to pieces object.
// ******************************************
Track.prototype.fillPiecesData = function (piecesData) {
    this.pieces = [];

    for (var i = 0; i < piecesData.length; i++) {
        var piece = {};
        piece.index = i;
        // Fill missing angle data
        piece.angle = (typeof(piecesData[i].angle) === 'undefined')? 0 : piecesData[i].angle;
        // Fill missing switch data
        piece.switch = (typeof(piecesData[i].switch) === 'undefined')? false : piecesData[i].switch;

        // Get piece lengths and radiuses for different lanes
        piece.laneLength = [];
        piece.laneRadius = [];
        piece.baseRadius = piecesData[i].radius || Infinity;

        // Tracktion data
        piece.baseLaneSafeSpeed =[];
        piece.laneSafeSpeedModifier =[];
        piece.maxSlipAngle = [];
        piece.laneSafeSpeed = [];

        for (var l = 0; l < this.lanes.length; l++) {

            piece.maxSlipAngle[l] = 0;
            piece.laneSafeSpeedModifier[l] = 1;

            // Fill missing radius data
            piece.laneRadius[l] = (typeof(piecesData[i].radius) === 'undefined')? Infinity : piecesData[i].radius - this.lanes[l].offset * sign(piece.angle);

            if (piece.angle !== 0) {;
                if ( piece.laneRadius[l] <= 40 ) {piece.laneSafeSpeedModifier[l] = 0.97}
                if ( piece.laneRadius[l] > 40 ) {piece.laneSafeSpeedModifier[l] = 1.000}
                if ( piece.laneRadius[l] > 60 ) {piece.laneSafeSpeedModifier[l] = 1.005}
                if ( piece.laneRadius[l] >= 90 ) {piece.laneSafeSpeedModifier[l] = 1.010}
                if ( piece.laneRadius[l] >= 130 ) {piece.laneSafeSpeedModifier[l] = 1.018}
                if ( piece.laneRadius[l] >= 190 ) {piece.laneSafeSpeedModifier[l] = 1.02}

                if ( Math.abs(piece.angle) < 30 ) {piece.laneSafeSpeedModifier[l] *= 1.02}

                piece.baseLaneSafeSpeed[l] = Math.sqrt( 9.8 * this.baseFriction * piece.laneRadius[l]  );
                piece.laneSafeSpeed[l] = piece.baseLaneSafeSpeed[l] * piece.laneSafeSpeedModifier[l];

                piece.laneLength[l] = Math.abs(piece.angle) * Math.PI * piece.laneRadius[l] / 180.0;
            }
            else {
                piece.laneLength[l] = piecesData[i].length;
                piece.baseLaneSafeSpeed[l] = 100000000;
                piece.laneSafeSpeed[l] = piece.baseLaneSafeSpeed[l];
            }
        }


        // Push to array
        this.pieces[i] = piece;
    }
};

/*
Track.prototype.getBezierLength = function(x, y) {
    var factor = 0.125;
    var len = 94;

    x = [0, len*factor, len-len*factor, len];
    y = [10, 10, -10, -10];

    var newX = x[0];
    var newY = y[0];
    var l = 0;
    var resolution = 0.000001;


    for (var t = 0; t <= 1.0 + resolution; t+=resolution) {
        var lastX = newX;
        var lastY = newY;
        newX = bezier(x, t);
        newY = bezier(y, t);
        l += Math.sqrt(Math.pow(newX-lastX, 2) + Math.pow(newY-lastY, 2));


        //debug(t+";"+newX+";"+newY);
    }
    debug (l);
}
*/

// ******************************************
// Returns lanes[{offset:-10}, {offset:10}]
// ******************************************
Track.prototype.getLanesData = function (lanesData) {
    this.lanes = [];
    for (var i = 0; i < lanesData.length; i++) {
        var lane = {};
        lane.offset = lanesData[i].distanceFromCenter;
        this.lanes[lanesData[i].index] = lane;
    }
};

Track.prototype.getPieceIndexByOffset = function (currentIndex, offset) {
    offset = (offset < 0)? this.pieces.length + (currentIndex + offset)%this.pieces.length : offset;
    var index = (currentIndex + offset)%this.pieces.length;
    return index;
}

Track.prototype.getLongestStraight = function () {
    this.maxStraightLen = 0;
    this.maxStraightStart = 0;
    this.maxStraightEnd = 0;

    var straightLen = 0;
    var straightEnd = 0;
    for (var p = 0; p < this.pieces.length * 2; p++) {

        var pp = this.getPieceIndexByOffset(0, p);
        if (this.pieces[pp].angle == 0) {
            straightLen = this.pieces[pp].laneLength[0];
            var s;
            for (s = 1; s <= this.pieces.length; s++) {
                if (this.pieces[this.getPieceIndexByOffset(pp, s)].angle  == 0)
                {
                    straightLen = straightLen + this.pieces[pp].laneLength[0];
                    straightEnd = this.getPieceIndexByOffset(pp, s);
                }
                else {
                    break;
                }

            }
            if (straightLen > this.maxStraightLen) {
                this.maxStraightLen = straightLen;
                this.maxStraightStart = p;
                this.maxStraightEnd = straightEnd;
            }
        }
    }
    debug("Longest straight: "+this.maxStraightStart +".."+this.maxStraightEnd+", "+this.maxStraightLen);
}





module.exports = Track;