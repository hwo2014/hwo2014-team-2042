var Race = function (raceData) {
    this.laps = (typeof raceData.laps !== undefined)? raceData.laps : 0;
    this.maxLapTimeMs = (typeof raceData.maxLapTimeMs !== undefined)? raceData.maxLapTimeMs : 0;
    this.quickRace = (typeof raceData.quickRace)? raceData.quickRace : false;
    this.started = false;
}

module.exports = Race;